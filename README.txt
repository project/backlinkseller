********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Backlinkseller
Author: Alexander Bogomolov <alexander@bogomolov.de>
Drupal: 7
********************************************************************


DESCRIPTION:

This module allow user to embed the Backlinkseller code without using
the PHP filter module.

It provides a new block for backlinks and for the affiliate program.
This module hides all indicators, that you are using this service.

********************************************************************

FEATURES:

- Configurate the output of the ads and set the debug mode.
- Choose between the default backlinkseller.de banner for the affiliate
  program.

********************************************************************

INSTALLATION:

1.  Copy module to your module directory and then enable on the admin modules
    page.

2.  Go to the configuration page admin/config/services/backlinkseller and set
    your Backlinkseller ID. Set the Backlinkseller block to any region of your
    theme.

********************************************************************

WARNING:

Search engines do not allow the use of link selling! Use at your own risk!
